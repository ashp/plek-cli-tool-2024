<!-- @format -->

# Plek CLI Tool

## Install Dependencies

`npm install`

## Run The App

You can place any .text files that you want to analyze within the `dist/text-files` directory.

In the project directory, you can run:

`npm run start:app text-files <number>`

The report will be printed in the console.
